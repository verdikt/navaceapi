﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OperatorTest.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   The operator test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Navace.Controllers;
    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities;

    using Newtonsoft.Json;

    /// <summary>
    /// The operator test.
    /// </summary>
    public class OperatorTest
    {
        /// <summary>
        /// The operator get test.
        /// </summary>
        [TestMethod]
        public void OperatorGetTest()
        {
            var op = new StringReader(Properties.Resources.Operator);
            var ope = new Mock<OperatorRepository>();
            ope.Setup(service => service.GetById(1402))
                        .Returns(JsonConvert.DeserializeObject<Operator>(op.ReadToEnd()));

            var controller = new OperatorController(ope.Object);
            var value = controller.Get();
            Assert.IsTrue(value.Jobs.Any());
            //var value2 = controller.Get();
            //Assert.IsFalse(((string)JsonConvert.SerializeObject(value2)).Contains("token"));
        }
    }
}
