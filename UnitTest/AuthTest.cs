﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthTest.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   The unit test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTest
{
    using System.IO;
    using System.Reflection;
    using System.Resources;
    using System.Xml.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Navace.Controllers;
    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities;

    using Newtonsoft.Json;

    /// <summary>
    /// The unit test.
    /// </summary>
    [TestClass]
    public class AuthTest
    {
        /// <summary>
        /// The authentication test method.
        /// </summary>
        [TestMethod]
        public void TestAuthGetToken()
        {
            var user = new Mock<UserRepository>();
            user.Setup(service => service.GetById(1402))
                        .Returns(new User(XDocument.Load(new StringReader(Properties.Resources.UserInfo)), 1402));

            var controller = new AuthController(user.Object);
            var value = controller.Get(1402, "781cbc549ac6ba52a10c939370f0d559f8cee1ce9c068d34f773cd04abb9c6afc1b0ebd56792debd9b71c7d538dbfe912fabcf898acb288421b35e77f9b99e1d", "test", "test", "test@test.com", "00:00", "test");
            Assert.IsTrue(((string)JsonConvert.SerializeObject(value)).Contains("token")); 
            var value2 = controller.Get(1402, string.Empty, "test", "test", "test@test.com", "00:00", "test");
            Assert.IsFalse(((string)JsonConvert.SerializeObject(value2)).Contains("token"));
        }
    }
}
