﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   The user.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Dynamic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Microsoft.SqlServer.Server;

    /// <summary>
    /// The user.
    /// </summary>
    public class User
    {
        /// <summary>
        /// The get id by token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="Exception">Condition. </exception>
        public static int GetIdByToken(string token)
        {
            var file = Path.Combine(Path.GetTempPath(), token);
            if (!File.Exists(file))
            {
                throw new Exception(string.Format("Auth Refused, file not found :{0}", file));
            }
            var idtxt = File.ReadAllText(file);
            int id = 0;
            try
            {
               id = Convert.ToInt32(idtxt);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Conversion failed, {0}: {1}", idtxt, ex.Message));
            }

            return id;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="userInfo">
        /// The user info.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        public User(XDocument userInfo, int id)
        {
            this.UserInfo = userInfo;
            this.Id = id;
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Gets the username.
        /// </summary>
        public string Username
        {
            get
            {
                var element = this.Root.Element("Username");
                return element != null ? element.Value : null;
            }
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        public string Password
        {
            get
            {
                var element = this.Root.Element("Password");
                return element != null ? element.Value : null;
            }
        }

        /// <summary>
        /// Gets or sets the user info.
        /// </summary>
        public XDocument UserInfo { get; set; }

        /// <summary>
        /// Gets the root.
        /// </summary>
        private XElement Root
        {
            get
            {
                return this.UserInfo.Root;
            }
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="addressMac">
        /// The address mac.
        /// </param>
        /// <param name="guid">
        /// The identifier.
        /// </param>
        public void Configure(string version, string addressMac, string guid)
        {
            this.Guid = guid;

            var userActive = this.Root.Element("UserActive");
            if (userActive != null)
            {
                userActive.Value = "True";
            }

            var appDetails = this.Root.Elements("AppDetails")
                .First(
                    p =>
                        {
                            var appName = p.Element("AppName");
                            return appName != null && appName.Value == "NavCheck";
                        });
            var versionInstalled = appDetails.Element("VersionInstalled");
            if (versionInstalled != null)
            {
                versionInstalled.Value = version;
            }

            var activationDate = appDetails.Element("ActivationDate");
            if (activationDate != null)
            {
                activationDate.Value = DateTime.Now.ToShortDateString();
            }

            var activationMac = appDetails.Element("ActivationMAC");
            if (activationMac != null)
            {
                activationMac.Value = addressMac;
            }
        }
    }
}
