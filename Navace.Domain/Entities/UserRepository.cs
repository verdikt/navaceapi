﻿namespace Navace.Domain.Entities
{
    using System;
    using System.Linq;

    using Navace.Domain.Abstractions;

    /// <summary>
    /// The user repository.
    /// </summary>
    public class UserRepository : IRepository<User>, IDisposable
    {
        public int Insert(User entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(User entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Save(User entity)
        {
            var navaceData = new NavaceRepositoryParser(entity.Id);

            navaceData.SaveUserInfo(entity);
        }

        public IQueryable<User> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        public User GetById(int id)
        {
            var navaceData = new NavaceRepositoryParser(id);

            var userItem = navaceData.GetUserInfo();

            return userItem;

        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
    }


}