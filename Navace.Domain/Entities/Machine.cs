﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Machine.cs" company="Verdikt">
//   
// </copyright>
// <summary>
//   Defines the Machine type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Navace.Domain.Entities
{
    using LINQtoCSV;

    /// <summary>
    /// The machine.
    /// </summary>
    public class Machine
    {
        [CsvColumn(FieldIndex = 0)]
        public string MachineId;
        [CsvColumn(FieldIndex = 1)]
        public string ManufacturerId;
        [CsvColumn(FieldIndex = 2)]
        public string Model;
        [CsvColumn(FieldIndex = 3)]
        public string SerialNumber;
        [CsvColumn(FieldIndex = 4)]
        public string MachineArrangement;
        [CsvColumn(FieldIndex = 5)]
        public string EngineSerialNumber;
        [CsvColumn(FieldIndex = 6)]
        public string TransmissionSerialNumber;
        [CsvColumn(FieldIndex = 7)]
        public string CustomerId;
        [CsvColumn(FieldIndex = 8)]
        public string MachineClassId;
        [CsvColumn(FieldIndex = 9)]
        public string OwnershipSMU;
        [CsvColumn(FieldIndex = 10)]
        public double DailyUsage;
        [CsvColumn(FieldIndex = 11)]
        public string CommenceSMU;
        [CsvColumn(FieldIndex = 12)]
        public string CommenceDate;
        [CsvColumn(FieldIndex = 13)]
        public string ScheduleGroup;
        [CsvColumn(FieldIndex = 14)]
        public string FamilyId;
        [CsvColumn(FieldIndex = 15)]
        public string ManagementFree;
        [CsvColumn(FieldIndex = 16)]
        public double CPHBudget;
        [CsvColumn(FieldIndex = 17)]
        public double UtilisationBudget;
        [CsvColumn(FieldIndex = 18)]
        public double AvailabilityBudget;
        [CsvColumn(FieldIndex = 19)]
        public string NewHourMeter;
        [CsvColumn(FieldIndex = 20)]
        public string NotActive;
        [CsvColumn(FieldIndex = 21)]
        public string HourMeterReplaced;
        [CsvColumn(FieldIndex = 22)]
        public string UnitMeasurement;
        [CsvColumn(FieldIndex = 23)]
        public string EngineArrNumber;
        [CsvColumn(FieldIndex = 24)]
        public string TransmissionArrNumber;
        [CsvColumn(FieldIndex = 25)]
        public string EngineMake;
        [CsvColumn(FieldIndex = 26)]
        public string EngineModel;
        [CsvColumn(FieldIndex = 27)]
        public string TransmissionMake;
        [CsvColumn(FieldIndex = 28)]
        public string TransmissionModel;
        [CsvColumn(FieldIndex = 29)]
        public string HydraulicsModel;
        [CsvColumn(FieldIndex = 30)]
        public string HydraulicSerialNo;
        [CsvColumn(FieldIndex = 31)]
        public string PlantNo;
        [CsvColumn(FieldIndex = 32)]
        public string AccountCode;
        [CsvColumn(FieldIndex = 33)]
        public string NavaceNo;
        [CsvColumn(FieldIndex = 34)]
        public string OperatorName;
        [CsvColumn(FieldIndex = 35)]
        public string OperatorMobile;
        [CsvColumn(FieldIndex = 36)]
        public string FuelFrenquencyMach;
        [CsvColumn(FieldIndex = 37)]
        public string NextFuelDate;
        [CsvColumn(FieldIndex = 38)]
        public string Pitch;
        [CsvColumn(FieldIndex = 39)]
        public string PinDiameter;
        [CsvColumn(FieldIndex = 40)]
        public string Spread;
        [CsvColumn(FieldIndex = 41)]
        public string StoreId;
        [CsvColumn(FieldIndex = 42)]
        public string EngineOil;
        [CsvColumn(FieldIndex = 43)]
        public string TransOil;
        [CsvColumn(FieldIndex = 44)]
        public string DiffOil;
        [CsvColumn(FieldIndex = 45)]
        public string FinalDriveOil;
        [CsvColumn(FieldIndex = 46)]
        public string HydraulicOil;
        [CsvColumn(FieldIndex = 47)]
        public string TransferCaseOil;
        [CsvColumn(FieldIndex = 48)]
        public string RegistrationNumber;
        [CsvColumn(FieldIndex = 49)]
        public string PicturePath;
        [CsvColumn(FieldIndex = 50)]
        public string IsAttachement;
        [CsvColumn(FieldIndex = 51)]
        public string NavaceDomain2;
    }
}