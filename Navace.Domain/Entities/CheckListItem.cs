﻿using LINQtoCSV;
namespace Navace.Domain.Entities
{
    public class CheckListItem
    {
        [CsvColumn(FieldIndex = 0)]
        public int ChecklistId;
        [CsvColumn(FieldIndex = 1)]
        public int MachineId;
        [CsvColumn(FieldIndex = 2)]
        public string ComponentCode;
        [CsvColumn(FieldIndex = 3)]
        public int Sequence;
        [CsvColumn(FieldIndex = 4)]
        public int Priority;
        [CsvColumn(FieldIndex = 5)]
        public string Memo;
    }
}