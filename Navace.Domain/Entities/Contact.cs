﻿using LINQtoCSV;
namespace Navace.Domain.Entities
{
    public class Contact
    {

        [CsvColumn(FieldIndex = 0)]
        public string ContactId;

        [CsvColumn(FieldIndex = 1)]
        public string unknow;

        [CsvColumn(FieldIndex = 2)]
        public string Firstname;

        [CsvColumn(FieldIndex = 3)]
        public string Lastname;

        [CsvColumn(FieldIndex = 4)]
        public string Phone;
    }
}