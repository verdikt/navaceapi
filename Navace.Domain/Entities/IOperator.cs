﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IOperator.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the IOperator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities
{
    using System.Collections.Generic;

    using Navace.Domain.Entities.Upload;

    /// <summary>
    /// The Operator interface.
    /// </summary>
    public interface IOperator
    {
        /// <summary>
        /// Gets or sets the check list items.
        /// </summary>
        List<CheckListItem> CheckListItems { get; set; }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        List<Contact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the jobs.
        /// </summary>
        List<Job> Jobs { get; set; }

        /// <summary>
        /// Gets or sets the machines.
        /// </summary>
        List<Machine> Machines { get; set; }

        /// <summary>
        /// Gets or sets the machine schedules.
        /// </summary>
        List<MachineSchedule> MachineSchedules { get; set; }

        /// <summary>
        /// Gets or sets the master sites.
        /// </summary>
        List<MasterSite> MasterSites { get; set; }

        /// <summary>
        /// Gets or sets the site machines.
        /// </summary>
        List<SiteMachine> SiteMachines { get; set; }

        /// <summary>
        /// Gets or sets the timesheet activities.
        /// </summary>
        List<TimesheetActivity> TimesheetActivities { get; set; }

        /// <summary>
        /// Gets or sets the timesheet supervisors.
        /// </summary>
        List<TimesheetSupervisor> TimesheetSupervisors { get; set; }

        /// <summary>
        /// Gets or sets the timesheet tasks.
        /// </summary>
        List<TimesheetTask> TimesheetTasks { get; set; }

        /// <summary>
        /// Gets or sets the incident report.
        /// </summary>
        List<dynamic> IncidentReport { get; set; }

        /// <summary>
        /// Gets or sets the manufacturers.
        /// </summary>
        List<Manufacturer> Manufacturers { get; set; }
    }
}
