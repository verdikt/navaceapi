﻿using LINQtoCSV;

namespace Navace.Domain.Entities
{
    public class MasterSite
    {
        [CsvColumn(FieldIndex = 0)]
        public int MasterSiteId;
        [CsvColumn(FieldIndex = 1)]
        public int SiteCurrent;
        [CsvColumn(FieldIndex = 2)]
        public int PrincipalId;
        [CsvColumn(FieldIndex = 3)]
        public string SiteName;
        [CsvColumn(FieldIndex = 4)]
        public string SiteAddress;
        [CsvColumn(FieldIndex = 5)]
        public string SiteSuburb;
        [CsvColumn(FieldIndex = 6)]
        public string SiteState;
        [CsvColumn(FieldIndex = 7)]
        public string SitePostcode;
        [CsvColumn(FieldIndex = 8)]
        public string SitePhone;
        [CsvColumn(FieldIndex = 9)]
        public string SiteFax;
        [CsvColumn(FieldIndex = 10)]
        public int SiteContact;
        [CsvColumn(FieldIndex = 11)]
        public string SiteContactMobile;
        [CsvColumn(FieldIndex = 12)]
        public string SiteStart;
        [CsvColumn(FieldIndex = 13)]
        public string SiteFinish;
        [CsvColumn(FieldIndex = 14)]
        public double DaysOperation;
        [CsvColumn(FieldIndex = 15)]
        public double MonFriHours;
        [CsvColumn(FieldIndex = 16)]
        public double SatHours;
        [CsvColumn(FieldIndex = 17)]
        public double SunHours;
        [CsvColumn(FieldIndex = 18)]
        public double Dist_From_Base;
        [CsvColumn(FieldIndex = 19)]
        public string MonFriStart;
        [CsvColumn(FieldIndex = 20)]
        public string MonFriFinish;
        [CsvColumn(FieldIndex = 21)]
        public string SatStart;
        [CsvColumn(FieldIndex = 22)]
        public string SatFinish;
        [CsvColumn(FieldIndex = 23)]
        public string SunStart;
        [CsvColumn(FieldIndex = 24)]
        public string SunFinish;
        [CsvColumn(FieldIndex = 25)]
        public int FuelCycle;
        [CsvColumn(FieldIndex = 26)]
        public string NextFuelDate;
        [CsvColumn(FieldIndex = 27)]
        public int SiteContactSafety;
        [CsvColumn(FieldIndex = 28)]
        public int SiteContactEnvironment;
        [CsvColumn(FieldIndex = 29)]
        public int SiteContactMaintenance;
        [CsvColumn(FieldIndex = 30)]
        public string Unknown1;
        [CsvColumn(FieldIndex = 31)]
        public string Unknown2;
    }
}