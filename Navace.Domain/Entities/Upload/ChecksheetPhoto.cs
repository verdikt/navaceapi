﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChecksheetPhoto.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the ChecksheetPhoto type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities.Upload
{
    using LINQtoCSV;

    /// <summary>
    /// The check sheet photo.
    /// </summary>
    public class Photo
    {
        /// <summary>
        /// Gets or sets the check sheet name.
        /// </summary>
        [CsvColumn(FieldIndex = 0)]
        public string CspChecksheetName { get; set; }

        /// <summary>
        /// Gets or sets the item number.
        /// </summary>
        [CsvColumn(FieldIndex = 2)]
        public string CspItemNumber { get; set; }

        /// <summary>
        /// Gets or sets the photo name.
        /// </summary>
        [CsvColumn(FieldIndex = 1)]
        public string CspPhotoName { get; set; }

        /// <summary>
        /// Gets or sets the check sum.
        /// </summary>
        [CsvColumn(FieldIndex = 3)]
        public string CspCheckSum { get; set; }

    }
}
