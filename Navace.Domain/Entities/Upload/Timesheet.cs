﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimesheetActivity.cs" company="Verdikt">
//   
// </copyright>
// <summary>
//   Defines the TimesheetActivity type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities.Upload
{
    using LINQtoCSV;
    /// <summary>
    /// The timesheet activity.
    /// </summary>
    public class Timesheet
    {
        [CsvColumn(FieldIndex = 0)]
        public string TS_Name { get; set; }

        [CsvColumn(FieldIndex = 1)]
        public string TS_Entry { get; set; }

        [CsvColumn(FieldIndex = 2)]
        public int TS_ContactID { get; set; } 

        [CsvColumn(FieldIndex = 3)]
        public int TS_MasterSiteID { get; set; }

        [CsvColumn(FieldIndex = 4)]
        public int TS_MachineID { get; set; }

        [CsvColumn(FieldIndex = 5)]
        public int TS_JobID { get; set; }

        [CsvColumn(FieldIndex = 6)]
        public string TS_Date { get; set; }

        [CsvColumn(FieldIndex = 7)]
        public string TS_StartTime { get; set; }

        [CsvColumn(FieldIndex = 8)]
        public string TS_FinishTime { get; set; }

        [CsvColumn(FieldIndex = 9)]
        public int TS_TotalTime { get; set; }

        [CsvColumn(FieldIndex = 10)]
        public string TS_Task { get; set; }

        [CsvColumn(FieldIndex = 11)]
        public string TS_Activity { get; set; }

        [CsvColumn(FieldIndex = 12)]
        public string TS_Description { get; set; }

        [CsvColumn(FieldIndex = 13)]
        public string TS_RateCode { get; set; }

        [CsvColumn(FieldIndex = 14)]
        public string TS_Latitude { get; set; }

        [CsvColumn(FieldIndex = 15)]
        public string TS_Longitude { get; set; }

        [CsvColumn(FieldIndex = 16)]
        public string TS_TimeStamp { get; set; }

        [CsvColumn(FieldIndex = 17)]
        public int TS_IsLocked { get; set; }

        [CsvColumn(FieldIndex = 18)]
        public int TS_IsDeleted { get; set; }

        [CsvColumn(FieldIndex = 19)]
        public int TS_IsSubmitted { get; set; }

        [CsvColumn(FieldIndex = 20)]
        public int TS_IsChecked { get; set; }

        [CsvColumn(FieldIndex = 21)]
        public string TS_SupervisorID { get; set; }

        [CsvColumn(FieldIndex = 22)]
        public int TS_IsDisputed { get; set; }

        [CsvColumn(FieldIndex = 23)]
        public int TS_IsApproved { get; set; }

        [CsvColumn(FieldIndex = 24)]
        public int TS_IsReconciled { get; set; }

        [CsvColumn(FieldIndex = 25)]
        public string TS_CheckSum { get; set; }
    }
}