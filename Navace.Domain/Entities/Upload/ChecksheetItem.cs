﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChecksheetItem.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the ChecksheetItem type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Navace.Domain.Entities.Upload
{
    using System.Collections.Generic;

    using LINQtoCSV;

    /// <summary>
    ///     The check sheet item.
    /// </summary>
    public class ChecksheetItem
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the check sum.
        /// </summary>
        [CsvColumn(FieldIndex = 8)]
        public string Csi_CheckSum { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        [CsvColumn(FieldIndex = 7)]
        public string Csi_TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [CsvColumn(FieldIndex = 5)]
        public string Csi_Comment { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [CsvColumn(FieldIndex = 3)]
        public string Csi_Description { get; set; }

        /// <summary>
        /// Gets or sets the photo count.
        /// </summary>
        [CsvColumn(FieldIndex = 6)]
        public string Csi_PhotoCount { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        [CsvColumn(FieldIndex = 4)]
        public string Csi_Status { get; set; }

        /// <summary>
        /// Gets or sets the check list item ref.
        /// </summary>
        [CsvColumn(FieldIndex = 2)]
        public string Csi_CheckListItemRef { get; set; }

        /// <summary>
        /// Gets or sets the check sheet name.
        /// </summary>
        [CsvColumn(FieldIndex = 0)]
        public string Csi_ChecksheetName { get; set; }

        /// <summary>
        /// Gets or sets the item number.
        /// </summary>
        [CsvColumn(FieldIndex = 1)]
        public string Csi_ItemNumber { get; set; }

        #endregion
    }
}