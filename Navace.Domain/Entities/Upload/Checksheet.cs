﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Checksheet.cs" company="">
//   
// </copyright>
// <summary>
//   The checksheet.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Navace.Domain.Entities.Upload
{
    using System.Collections.Generic;

    using LINQtoCSV;

    /// <summary>
    ///     The check sheet.
    /// </summary>
    public class Checksheet
    {
        #region Public Properties
        /// <summary>
        ///  Gets or sets The name.
        /// </summary>
        [CsvColumn(FieldIndex = 0)]
        public string Cs_Name { get; set; }

        /// <summary>
        ///  Gets or sets The operator ref.
        /// </summary>
        [CsvColumn(FieldIndex = 1)]
        public string Cs_OperatorRef { get; set; }

        /// <summary>
        ///  Gets or sets The machine ref.
        /// </summary>
        [CsvColumn(FieldIndex = 2)]
        public string Cs_MachineRef { get; set; }

        /// <summary>
        ///  Gets or sets The master site reference.
        /// </summary>
        [CsvColumn(FieldIndex = 3)]
        public string Cs_MasterSiteRef { get; set; }

        /// <summary>
        /// Gets or sets The date.
        /// </summary>
        [CsvColumn(FieldIndex = 4)]
        public string Cs_Date { get; set; }

        /// <summary>
        ///  Gets or sets The time.
        /// </summary>
        [CsvColumn(FieldIndex = 5)]
        public string Cs_Time { get; set; }

        /// <summary>
        ///  Gets or sets The s m u.
        /// </summary>
        [CsvColumn(FieldIndex = 6)]
        public string Cs_Smu { get; set; }

        /// <summary>
        ///  Gets or sets The operator ok.
        /// </summary>
        [CsvColumn(FieldIndex = 7)]
        public string Cs_OperatorOk { get; set; }

        /// <summary>
        /// Gets or sets The component code.
        /// </summary>
        [CsvColumn(FieldIndex = 8)]
        public string Cs_ComponentCode { get; set; }

        /// <summary>
        /// Gets or sets The item count.
        /// </summary>
        [CsvColumn(FieldIndex = 9)]
        public string Cs_ItemCount { get; set; }

        /// <summary>
        /// Gets or sets The machine ok.
        /// </summary>
        [CsvColumn(FieldIndex = 10)]
        public string Cs_MachineOk { get; set; }

        /// <summary>
        /// Gets or sets The machine ok.
        /// </summary>
        [CsvColumn(FieldIndex = 11)]
        public string Cs_Latitude { get; set; }

        /// <summary>
        /// Gets or sets The machine ok.
        /// </summary>
        [CsvColumn(FieldIndex = 12)]
        public string Cs_Longitude { get; set; }

        /// <summary>
        /// Gets or sets The machine ok.
        /// </summary>
        [CsvColumn(FieldIndex = 13)]
        public string Cs_JobRef { get; set; } 

        /// <summary>
        ///  Gets or sets The check sum.
        /// </summary>
        [CsvColumn(FieldIndex = 14)]
        public string Cs_CheckSum { get; set; }

        #endregion
    }
}