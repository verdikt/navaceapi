﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IncidentReport.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the IncidentReport type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities.Upload
{
    using LINQtoCSV;

    /// <summary>
    /// The incident report.
    /// </summary>
    public class IncidentReport
    {
        /// <summary>
        /// Gets or sets the incident_ name.
        /// </summary>
        [CsvColumn(FieldIndex = 0)]
        public string Incident_Name { get; set; }

        /// <summary>
        /// Gets or sets the incident_ operator ref.
        /// </summary>
        [CsvColumn(FieldIndex = 1)]
        public string Incident_OperatorRef { get; set; }
        [CsvColumn(FieldIndex = 2)]
        public string Incident_MachineRef { get; set; }
        [CsvColumn(FieldIndex = 3)]
        public string Incident_MasterSiteRef { get; set; }
        [CsvColumn(FieldIndex = 4)]
        public string Incident_Date { get; set; }
        [CsvColumn(FieldIndex = 5)]
        public string Incident_Time { get; set; }
        [CsvColumn(FieldIndex = 6)]
        public string Incident_Latitude { get; set; }
        [CsvColumn(FieldIndex = 7)]
        public string Incident_Longitude { get; set; }
        [CsvColumn(FieldIndex = 8)]
        public string Incident_Category { get; set; }
        [CsvColumn(FieldIndex = 9)]
        public string Incident_Subcategory { get; set; }
        [CsvColumn(FieldIndex = 10)]
        public string Incident_Severity { get; set; }
        [CsvColumn(FieldIndex = 11)]
        public string Incident_ComponentCode { get; set; }
        [CsvColumn(FieldIndex = 12)]
        public string Incident_Description { get; set; }
        [CsvColumn(FieldIndex = 13)]
        public string Incident_Response { get; set; }
        [CsvColumn(FieldIndex = 14)]
        public string Incident_ActionRequired { get; set; }
        [CsvColumn(FieldIndex = 15)]
        public string Incident_PhotoCount { get; set; }
        [CsvColumn(FieldIndex = 16)]
        public string Incident_CheckSum { get; set; }
    }
}
