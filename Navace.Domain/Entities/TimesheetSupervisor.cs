﻿using System.Collections.Generic;
using LINQtoCSV;

namespace Navace.Domain.Entities
{
    public class TimesheetSupervisor
    {
        [CsvColumn(FieldIndex = 0)] public string SupervisorID;
        [CsvColumn(FieldIndex = 1)] public string MasterSiteID;
        [CsvColumn(FieldIndex = 2)] public string FirstName;
        [CsvColumn(FieldIndex = 3)] public string LastName;
        [CsvColumn(FieldIndex = 4)] public string Mobile1;
        [CsvColumn(FieldIndex = 5)] public string Mobile2;
        [CsvColumn(FieldIndex = 6)] public string Phone1;
        [CsvColumn(FieldIndex = 7)] public string Phone2;
        [CsvColumn(FieldIndex = 8)] public string Fax;
        [CsvColumn(FieldIndex = 9)] public string EmailAddress;
        [CsvColumn(FieldIndex = 10)] public string PositionID;
        [CsvColumn(FieldIndex = 11)] public string SupplierID;
    }
}