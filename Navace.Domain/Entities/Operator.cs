﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Operator.cs" company="Verdikt">
//   
// </copyright>
// <summary>
//   The operator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities
{
    using System.Collections.Generic;

    using Navace.Domain.Entities.Upload;

    /// <summary>
    /// The operator.
    /// </summary>
    public class Operator : IOperator
    {
        /// <summary>
        /// Gets or sets the check list items.
        /// </summary>
        public List<CheckListItem> CheckListItems { get; set; }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        public List<Contact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the jobs.
        /// </summary>
        public List<Job> Jobs { get; set; }

        /// <summary>
        /// Gets or sets the machines.
        /// </summary>
        public List<Machine> Machines { get; set; }

        /// <summary>
        /// Gets or sets the machine schedules.
        /// </summary>
        public List<MachineSchedule> MachineSchedules { get; set; }

        /// <summary>
        /// Gets or sets the master sites.
        /// </summary>
        public List<MasterSite> MasterSites { get; set; }

        /// <summary>
        /// Gets or sets the site machines.
        /// </summary>
        public List<SiteMachine> SiteMachines { get; set; }

        /// <summary>
        /// Gets or sets the timesheet activities.
        /// </summary>
        public List<TimesheetActivity> TimesheetActivities { get; set; }

        /// <summary>
        /// Gets or sets the timesheet supervisors.
        /// </summary>
        public List<TimesheetSupervisor> TimesheetSupervisors { get; set; }

        /// <summary>
        /// Gets or sets the timesheet tasks.
        /// </summary>
        public List<TimesheetTask> TimesheetTasks { get; set; }

        /// <summary>
        /// Gets or sets the incident report.
        /// </summary>
        public List<dynamic> IncidentReport { get; set; }

        /// <summary>
        /// Gets or sets the component code.
        /// </summary>
        public List<ComponentCode> ComponentCodes { get; set; }

        /// <summary>
        /// Gets or sets the manufacturers.
        /// </summary>
        public List<Manufacturer> Manufacturers { get; set; }

        public bool TimesheetActivated { get; set; }

    }
}
