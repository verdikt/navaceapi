﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimesheetTask.cs" company="Verdikt">
//   
// </copyright>
// <summary>
//   Defines the TimesheetTask type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities
{
    using System.Collections.Generic;

    using LINQtoCSV;

    /// <summary>
    /// The timesheet task.
    /// </summary>
    public class TimesheetTask
    {
        [CsvColumn(FieldIndex = 0)] public string TaskID;
        [CsvColumn(FieldIndex = 1)] public string MasterSiteID;
        [CsvColumn(FieldIndex = 2)] public string TaskCustomerTaskCode;
        [CsvColumn(FieldIndex = 3)] public string TaskDescription;

    }
}