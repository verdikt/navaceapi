﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteMachine.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the SiteMachine type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities
{
    using LINQtoCSV;

    /// <summary>
    /// The site machine.
    /// </summary>
    public class SiteMachine
    {
        /// <summary>
        /// The site machine id.
        /// </summary>
        [CsvColumn(FieldIndex = 0)]
        public int SiteMachineId;

        /// <summary>
        /// The site id.
        /// </summary>
        [CsvColumn(FieldIndex = 1)]
        public string SiteName;

        /// <summary>
        /// The master site id.
        /// </summary>
        [CsvColumn(FieldIndex = 2)]
        public int MasterSiteId;

        /// <summary>
        /// The site weekly scheduled hours.
        /// </summary>
        [CsvColumn(FieldIndex = 3)]
        public int SiteWeeklyScheduledHours;

        /// <summary>
        /// The machine id.
        /// </summary>
        [CsvColumn(FieldIndex = 4)]
        public int MachineId;

        /// <summary>
        /// The application.
        /// </summary>
        [CsvColumn(FieldIndex = 5)]
        public int Application;

        /// <summary>
        /// The function.
        /// </summary>
        [CsvColumn(FieldIndex = 6)]
        public int Function;

        /// <summary>
        /// The on site.
        /// </summary>
        [CsvColumn(FieldIndex = 7)]
        public string OnSite;

        /// <summary>
        /// The off site.
        /// </summary>
        [CsvColumn(FieldIndex = 8)]
        public string OffSite;

    }
}