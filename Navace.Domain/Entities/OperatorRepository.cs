﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OperatorRepository.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the OperatorRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Xml.Linq;

namespace Navace.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities.Upload;



    /// <summary>
    /// The operator repository.
    /// </summary>
    public class OperatorRepository : IRepository<Operator>, IDisposable
    {
        /// <summary>
        /// The file date format.
        /// </summary>
        public static readonly string FileDateFormat = "{0:yyyyMMddHHmmss}";

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// send exception
        /// </exception>
        public int Insert(Operator entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Operator entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Save(Operator entity)
        {

        }

        /// <summary>
        /// The save check sheet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="chk">
        /// The check sheet.
        /// </param>
        public void SaveChecksheet(int id, Checksheet chk, DateTime date)
        {
            var navaceData = new NavaceRepositoryParser(id);
            navaceData.SaveCsv(new [] {chk}, string.Format("NavCheck_{0}{1}CheckSheet.csv", id.ToString("D6"), string.Format(FileDateFormat, date)));
        }

        /// <summary>
        /// The save check sheet items.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="chk">
        /// The data.
        /// </param>
        public void SaveChecksheetItems(int id, List<ChecksheetItem> chk, DateTime date)
        {
            var navaceData = new NavaceRepositoryParser(id);
            navaceData.SaveCsv(chk, string.Format("NavCheck_{0}{1}ChecksheetItems.csv", id.ToString("D6"), string.Format(FileDateFormat, date)));
        }

        /// <summary>
        /// The save check sheet photos.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="chk">
        /// The data.
        /// </param>
        public void SaveChecksheetPhotos(int id, List<Photo> chk, DateTime date)
        {
            this.SaveCsvPhotos(id, chk, "NavCheck_CheckSheetPhotos", date);
        }

        /// <summary>
        /// The save report photos.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public void SaveReportPhotos(int id, List<Photo> data, DateTime date)
        {
            this.SaveCsvPhotos(id, data, "IncidentPhotos", date);
        }

        /// <summary>
        /// The save data photos.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        public void SaveCsvPhotos(int id, List<Photo> data, string name, DateTime date)
        {
            var navaceData = new NavaceRepositoryParser(id);
            navaceData.SaveCsv(data, string.Format("{2}{0}{1}.csv", id.ToString("D6"), string.Format(FileDateFormat, date), name));
        }

        /// <summary>
        /// The save report incident.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="rpt">
        /// The report.
        /// </param>
        public void SaveReportIncident(int id, IncidentReport rpt, DateTime date)
        {
            var navaceData = new NavaceRepositoryParser(id);
            navaceData.SaveCsv(new[] {rpt}, string.Format("NavCheck_{0}{1}_IncidentReport.csv", id.ToString("D6"), string.Format(FileDateFormat, date)));
        }

        /// <summary>
        /// The save timesheet.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="timesheet">
        /// The timesheet.
        /// </param>
        public void SaveTimesheet(int id, Timesheet timesheet, DateTime date)
        {
            var navaceData = new NavaceRepositoryParser(id);
            navaceData.SaveCsv(new[] {timesheet}, string.Format("NavCheck_{0}{1}TimeSheet.csv", id.ToString("D6"), string.Format(FileDateFormat, date)));
        }


        public IQueryable<Operator> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Operator"/>.
        /// </returns>
        public Operator GetById(int id)
        {
            var navaceData = new NavaceRepositoryParser(id);

            var element = navaceData.GetUserInfo().UserInfo.Root;
            var operatorItem = new Operator
                                    {
                                        CheckListItems =
                                            navaceData.CheckListItems()
                                            .ToList(),
                                        Contacts = navaceData.Contacts().ToList(),
                                        Jobs =
                                            navaceData.Jobs()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        MachineSchedules =
                                            navaceData.MachineSchedules()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        Machines =
                                            navaceData.Machines()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        MasterSites =
                                            navaceData.MasterSites()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        SiteMachines =
                                            navaceData.SiteMachines()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        TimesheetActivities =
                                            navaceData.TimesheetActivities()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        TimesheetSupervisors =
                                            navaceData.TimesheetSupervisors()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        TimesheetTasks =
                                            navaceData.TimesheetTasks()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        IncidentReport =
                                            navaceData.IncidentReport()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        ComponentCodes =
                                            navaceData.ComponentCodes()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                        TimesheetActivated =
                                            element.Elements("AppDetails")
                                                .Elements("TimesheetsActivated")
                                                .First()
                                                .Value == "True",
                                        Manufacturers =
                                            navaceData.Manufacturers()
                                            .CatchExceptions(ex => Debug.WriteLine(ex.Message)).ToList(),
                                    };
            

                return operatorItem;
            
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
    }


}