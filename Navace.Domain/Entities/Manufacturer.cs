﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Machine.cs" company="Verdikt">
//   
// </copyright>
// <summary>
//   Defines the Machine type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Navace.Domain.Entities
{
    using LINQtoCSV;

    /// <summary>
    /// The machine.
    /// </summary>
    public class Manufacturer
    {
        /// <summary>
        /// The manufacturer id.
        /// </summary>
        [CsvColumn(FieldIndex = 0)]
        public string ManufacturerId;

        /// <summary>
        /// The manufacturer.
        /// </summary>
        [CsvColumn(FieldIndex = 1)]
        public string Name;
    }
}