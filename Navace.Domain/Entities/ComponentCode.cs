﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LINQtoCSV;

namespace Navace.Domain.Entities
{
    public class ComponentCode
    {
        [CsvColumn(FieldIndex = 0)] public string ComponentId;
        [CsvColumn(FieldIndex = 1)] public string Name;
    }
}
