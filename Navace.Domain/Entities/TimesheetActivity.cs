﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimesheetActivity.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the TimesheetActivity type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities
{
    using LINQtoCSV;

    /// <summary>
    /// The timesheet activity.
    /// </summary>
    public class TimesheetActivity
    {
        /// <summary>
        /// The activity id.
        /// </summary>
        [CsvColumn(FieldIndex = 0)] 
        public string ActivityId;

        /// <summary>
        /// The activity category.
        /// </summary>
        [CsvColumn(FieldIndex = 3)] 
        public string ActivityCategory;

        /// <summary>
        /// The activity sequence.
        /// </summary>
        [CsvColumn(FieldIndex = 1)] 
        public string ActivitySequence;

        /// <summary>
        /// The activity name.
        /// </summary>
        [CsvColumn(FieldIndex = 2)] 
        public string ActivityName;
    }
}