﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MachineSchedule.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the MachineSchedule type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Domain.Entities
{
    using LINQtoCSV;

    /// <summary>
    /// The machine schedule.
    /// </summary>
    public class MachineSchedule
    {
        [CsvColumn(FieldIndex = 0)]
        public int MachineServiceScheduleId;
        [CsvColumn(FieldIndex = 1)]
        public int MachineId;
        [CsvColumn(FieldIndex = 2)]
        public string MasterSiteRef;
        [CsvColumn(FieldIndex = 3)]
        public int ServicemanId;
        [CsvColumn(FieldIndex = 4)]
        public string ComponentCode;
        [CsvColumn(FieldIndex = 5)]
        public string ScheduleType;
        [CsvColumn(FieldIndex = 6)]
        public string NextInterval;
        [CsvColumn(FieldIndex = 7)]
        public string ScheduleSMU;
        [CsvColumn(FieldIndex = 8)]
        public string ScheduleDate;
        [CsvColumn(FieldIndex = 9)]
        public string JobCode;
        [CsvColumn(FieldIndex = 10)]
        public string ServiceComplete;
        [CsvColumn(FieldIndex = 11)]
        public int ScheduleGroupId;
    }
}