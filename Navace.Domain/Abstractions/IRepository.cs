﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Navace.Domain.Entities;

namespace Navace.Domain.Abstractions
{
    public interface IRepository<T>
    {
        int Insert(T entity);
        void Delete(T entity);
        void Save(T entity);
        IQueryable<T> GetAll();
        T GetById(int id);
        void Dispose();
    }
}