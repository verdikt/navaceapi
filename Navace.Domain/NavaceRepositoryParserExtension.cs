namespace Navace.Domain
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using LINQtoCSV;

    /// <summary>
    /// The repository parser extension.
    /// </summary>
    public static class NavaceRepositoryParserExtension
    {
        /// <summary>
        /// The log file name.
        /// </summary>
        private static readonly string LogFileName = string.Format(
                            "NavaceApi-{0}-{1}-{2}.log",
                            DateTime.Now.Day,
                            DateTime.Now.Month,
                            DateTime.Now.Year);

        /// <summary>
        /// The log file path.
        /// </summary>
        public static readonly string LogFilePath = Path.Combine(Path.GetTempPath(), LogFileName);

        /// <summary>
        /// The catch exceptions.
        /// </summary>
        /// <param name="src">
        /// The source.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <typeparam name="T">
        /// The type of the object.
        /// </typeparam>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public static IEnumerable<T> CatchExceptions<T>(this IEnumerable<T> src, Action<Exception> action = null)
        {
            using (var enumerator = src.GetEnumerator())
            {
                var next = true;

                while (next)
                {
                    try
                    {
                        next = enumerator.MoveNext();
                    }
                    catch (AggregatedException ex)
                    {
                        lock (ex)
                        {
                            foreach (var e in ex.m_InnerExceptionsList)
                            {
                                if (action != null)
                                {
                                    action(e);
                                }

                                File.AppendAllText(LogFilePath, string.Format("{0}: {1}\r\n", DateTime.Now.ToShortTimeString(), e.Message));
                            }
                        }

                        File.AppendAllText(LogFilePath, "-\r\n");
                        continue;
                    }
                    catch (Exception ex)
                    {
                        if (action != null)
                        {
                            action(ex);
                        }

                        lock (ex)
                        {
                            File.AppendAllText(LogFilePath, string.Format("{0}: {1}\r\n", DateTime.Now.ToShortTimeString(), ex.Message));
                        }

                        continue;
                    }

                    if (next)
                    {
                        yield return enumerator.Current;
                    }
                }
            }
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// Any type.
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<T> Add<T>(this IEnumerable<T> e, T value)
        {
            foreach (var cur in e)
            {
                yield return cur;
            }
            yield return value;
        }

        /// <summary>
        /// The file to save.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <typeparam name="T">
        /// Type of the data
        /// </typeparam>
        public static void SaveCsv<T>(this IEnumerable<T> values, string path)
        {
            var csvContext = new CsvContext();
            lock (csvContext)
            {
                using (var t = File.CreateText(path))
                {
                    csvContext.Write(
                        values,
                        t,
                        new CsvFileDescription()
                            {
                                IgnoreUnknownColumns = true
                            });
                }
            }
        }
    }
}