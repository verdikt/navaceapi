﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NavaceRepositoryParser.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the NavaceRepositoryParser type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Web;

namespace Navace.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using LINQtoCSV;
    using Navace.Domain.Entities;


    /// <summary>
    /// The repository parser.
    /// </summary>
    public class NavaceRepositoryParser
    {
        /// <summary>
        /// The _path.
        /// </summary>
        private readonly string path;

        /// <summary>
        /// The _upload path.
        /// </summary>
        private readonly string uploadPath;

        /// <summary>
        /// The operator path.
        /// </summary>
        private readonly string operatorPath;

        /// <summary>
        /// The log path.
        /// </summary>
        private readonly string logPath;

        /// <summary>
        /// The input file without header.
        /// </summary>
        private readonly CsvFileDescription inputFileWithoutHeader = new CsvFileDescription
        {
            SeparatorChar = ',',
            FirstLineHasColumnNames = false,
            EnforceCsvColumnAttribute = true,
            IgnoreUnknownColumns = true
        };

        /// <summary>
        /// The input file with headers.
        /// </summary>
        private readonly CsvFileDescription inputFileWithHeaders = new CsvFileDescription
        {
            SeparatorChar = ',',
            FirstLineHasColumnNames = true,
            EnforceCsvColumnAttribute = false,
            IgnoreUnknownColumns = true
        };

        /// <summary>
        /// The data context.
        /// </summary>
        private readonly CsvContext csvContext = new CsvContext();

        /// <summary>
        /// The operator id.
        /// </summary>
        private readonly int operatorId;

        /// <summary>
        /// Initializes a new instance of the <see cref="NavaceRepositoryParser"/> class.
        /// </summary>
        /// <param name="operatorId">
        /// The operator id.
        /// </param>
        /// <exception cref="DirectoryNotFoundException">
        /// Thrown if user directory doesn't exist.
        /// </exception>
        public NavaceRepositoryParser(int operatorId)
        {
            this.operatorId = operatorId;
            this.path = Path.Combine(
                ConfigurationManager.AppSettings["FSPath"],
                operatorId.ToString(CultureInfo.InvariantCulture));
            this.uploadPath = Path.Combine(
                ConfigurationManager.AppSettings["UploadPath"],
                string.Empty);
            this.operatorPath = Path.Combine(
                ConfigurationManager.AppSettings["FSPath"],
                ConfigurationManager.AppSettings["OperatorsFolderId"]);

            this.logPath = Path.Combine(
                HttpContext.Current.Server.MapPath("~/Logs"));


            if (!Directory.Exists(this.path))
            {
                throw new DirectoryNotFoundException();
            }

            if (!Directory.Exists(this.uploadPath))
            {
                Directory.CreateDirectory(this.uploadPath);
            }

            if (!Directory.Exists(this.logPath))
            {
                Directory.CreateDirectory(this.logPath);
            }

        }

        /// <summary>
        /// The save image.
        /// </summary>
        /// <param name="base64String">
        /// The base 64 string.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        public static void SaveImage(string base64String, string filePath)
        {
            var imageBytes = Convert.FromBase64String(base64String);
            using (var imageFile = new FileStream(filePath, FileMode.Create))
            {
                imageFile.Write(imageBytes, 0, imageBytes.Length);
                imageFile.Flush();
            }
        }

        /// <summary>
        /// The manufacturers.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<Manufacturer> Manufacturers()
        {
            return
                Directory.EnumerateFileSystemEntries(this.path, "NCManufacturer*.csv")
                    .SelectMany(chkLstFile =>
                        {
                            return this.csvContext.Read<Manufacturer>(chkLstFile, this.inputFileWithoutHeader);
                        }).Distinct().AsEnumerable();
        }

        /// <summary>
        /// The check list items.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<CheckListItem> CheckListItems()
        {
            return
                Directory.EnumerateFileSystemEntries(this.path, "NCCheckListItem*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<CheckListItem>(chkLstFile, this.inputFileWithoutHeader))
                    .Distinct().Where(p => p.ComponentCode == "7520" || p.ComponentCode == "7521").AsEnumerable();
        }

        /// <summary>
        /// The contacts.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<Contact> Contacts()
        {
            return
                Directory.EnumerateFileSystemEntries(this.operatorPath, "NCContact*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<Contact>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The jobs.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<Job> Jobs()
        {
            return
                Directory.EnumerateFileSystemEntries(this.path, "NCJob*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<Job>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The machine schedules.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<MachineSchedule> MachineSchedules()
        {
            return
                Directory.EnumerateFileSystemEntries(this.path, "NCMachineSchedule*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<MachineSchedule>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary> 
        /// The machines.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<Machine> Machines()
        {
            return
                Directory.EnumerateFileSystemEntries(this.path, "NCMachine*.csv")
                    .Where(p => !p.Contains("MachineSchedule"))
                    .SelectMany(chkLstFile => this.csvContext.Read<Machine>(chkLstFile, this.inputFileWithoutHeader))
                    .Distinct();
        }

        /// <summary>
        /// The master sites.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<MasterSite> MasterSites()
        {
            return
                Directory.EnumerateFileSystemEntries(this.path, "NCMasterSite*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<MasterSite>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The site machines.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<SiteMachine> SiteMachines()
        {
            return
                Directory.EnumerateFileSystemEntries(this.path, "NCSiteMachine*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<SiteMachine>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The timesheet activities.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TimesheetActivity> TimesheetActivities()
        {
            return
                Directory.EnumerateFileSystemEntries(this.operatorPath, "NCTimesheetActivities_*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<TimesheetActivity>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The timesheet supervisors.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TimesheetSupervisor> TimesheetSupervisors()
        {
            return
                Directory.EnumerateFileSystemEntries(this.operatorPath, "NCTimesheetSupervisors_*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<TimesheetSupervisor>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The timesheet tasks.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TimesheetTask> TimesheetTasks()
        {
            return
                Directory.EnumerateFileSystemEntries(this.operatorPath, "NCTimesheetTasks_*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<TimesheetTask>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The component codes.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<ComponentCode> ComponentCodes()
        {
            return
                Directory.EnumerateFileSystemEntries(this.operatorPath, "NCComponentcodes_*.csv")
                    .SelectMany(chkLstFile => this.csvContext.Read<ComponentCode>(chkLstFile, this.inputFileWithoutHeader)).Distinct();
        }

        /// <summary>
        /// The incident report.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<dynamic> IncidentReport()
        {
            var xml = XDocument.Load(Path.Combine(this.path, "IncidentReport.xml"));

            if (xml.Root != null)
            {
                return (from incidentCategory in xml.Root.Elements("IncidentCategory")
                        let subCategories = incidentCategory.Elements("SubCategory")
                        select
                            new
                                {
                                    Name = incidentCategory.Attribute("Name").Value,
                                    SubCategories = from subCat in subCategories
                                                    select
                                                        new
                                                            {
                                                                Name = subCat.Attribute("Name").Value,
                                                                DefaultSeverity = subCat.Element("DefaultSeverity").Value,
                                                                ComponentCode = subCat.Element("ComponentCode").Value
                                                            }
                                })
                    .AsEnumerable();
            }
            return null;
        }

        /// <summary>
        /// The get user info.
        /// </summary>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        public User GetUserInfo()
        {
            var userInfoFilePath = Path.Combine(this.path, "Navace_User_Profile.xml");
            if (!File.Exists(userInfoFilePath))
            {
                return null;
            }

            var doc = XDocument.Load(userInfoFilePath);
            return doc.Root != null ? new User(doc, this.operatorId) : null;
        }

        /// <summary>
        /// The save user info.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SaveUserInfo(User user)
        {
            var file = Path.Combine(Path.GetTempPath(), user.Guid);
            File.WriteAllText(file, user.Id.ToString(CultureInfo.InvariantCulture));

            var userInfoFileUploadPath = Path.Combine(path, "Navace_User_Profile.xml");
            user.UserInfo.Save(userInfoFileUploadPath);
            return true;
        }

        /// <summary>
        /// Save a collection into a file.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <typeparam name="T">
        /// Type of the data.
        /// </typeparam>
        public void SaveCsv<T>(IEnumerable<T> values, string filename)
        {
            values.SaveCsv(Path.Combine(uploadPath, filename));
            values.SaveCsv(Path.Combine(logPath, filename));
        }
    }
}
