﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthController.cs" company=Verdikt"">
//   2014
// </copyright>
// <summary>
//   The auth controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Controllers
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web.Http;

    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities;

    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The authentication controller.
    /// </summary>
    public class AuthController : ApiController
    {
        /// <summary>
        /// The _user repository.
        /// </summary>
        private readonly UserRepository userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthController"/> class.
        /// </summary>
        /// <param name="userRepository">
        /// The user repository.
        /// </param>
        public AuthController(UserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [HttpGet]
        public dynamic Get(string id, string token)
        {
            var userServer = this.userRepository.GetById(Convert.ToInt32(id));
            if (userServer == null)
            {
                return new { };
            }
            var userInfo = userServer;
            string hash;
            using (SHA512 shaM = new SHA512Managed())
            {
                hash =
                    BitConverter.ToString(
                        shaM.ComputeHash(Encoding.UTF8.GetBytes(userInfo.Username + userInfo.Password)))
                        .Replace("-", string.Empty)
                        .ToLowerInvariant();
            }
            if (token != hash)
            {
                return new { };
            }

            return new { token = hash };
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="firstname">
        /// The first name.
        /// </param>
        /// <param name="lastname">
        /// The last name.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="mac">
        /// The mac.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>dynamic</cref>
        ///     </see>
        ///     .
        /// </returns>
        [HttpGet]
        public dynamic Get(int id, string token, string firstname, string lastname, string email, string mac, string version = "5.0")
        {
            var userServer = this.userRepository.GetById(id);
            if (userServer == null)
            {
                return new { };
            }

            var userInfo = userServer;
            string hash;
            using (SHA512 shaM = new SHA512Managed())
            {
                hash =
                    BitConverter.ToString(
                        shaM.ComputeHash(Encoding.UTF8.GetBytes(userInfo.Username + userInfo.Password)))
                        .Replace("-", string.Empty)
                        .ToLowerInvariant();
            }

            if (token != hash)
            {
                return new { };
            }

            var guid = Guid.NewGuid().ToString();

            userInfo.Configure(version, mac, guid);

            this.userRepository.Save(userServer);
            return new { token = guid };
        }

        public void Set()
        {
            var id = File.ReadAllText(Path.Combine(Path.GetTempPath(), Request.Headers.GetValues(ConfigurationManager.AppSettings["Token"]).First()));
        }
    }
}
