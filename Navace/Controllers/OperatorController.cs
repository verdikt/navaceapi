﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OperatorController.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the OperatorController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Controllers
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.Http;

    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities;
    using user = Navace.Domain.Entities.User;

    /// <summary>
    /// The operator controller.
    /// </summary>
    public class OperatorController : ApiController
    {
        /// <summary>
        /// The _operator repository.
        /// </summary>
        private readonly OperatorRepository operatorRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="OperatorController"/> class.
        /// </summary>
        /// <param name="operatorRepository">
        /// The operator repository.
        /// </param>
        public OperatorController(OperatorRepository operatorRepository)
        {
            this.operatorRepository = operatorRepository;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="Operator"/>.
        /// </returns>
        public Operator Get()
        {
            var operatorItem = this.operatorRepository.GetById(user.GetIdByToken(Request.Headers.GetValues(ConfigurationManager.AppSettings["Token"]).First()));

            foreach (var item in operatorItem.Machines)
            {
                item.EngineMake =
                    operatorItem.Manufacturers.Where(p => p.ManufacturerId == item.ManufacturerId)
                        .Select(p => p.Name)
                        .FirstOrDefault() ?? "Unknown";
            }

            return operatorItem;
        }
    }
}
