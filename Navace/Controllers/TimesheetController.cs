﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportController.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the ReportController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Web.Http;

    using Navace.Domain;
    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities;
    using Navace.Domain.Entities.Upload;

    using user = Navace.Domain.Entities.User;

    /// <summary>
    /// The report controller.
    /// </summary>
    public class TimesheetController : ApiController
    {
        /// <summary>
        /// The operator repository.
        /// </summary>
        private readonly OperatorRepository operatorRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimesheetController"/> class. 
        /// </summary>
        /// <param name="operatorRepository">
        /// The operator Repository.
        /// </param>
        public TimesheetController(OperatorRepository operatorRepository)
        {
            this.operatorRepository = operatorRepository;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        public void Post(dynamic obj)
        {
            try
            {
                var id = user.GetIdByToken(Request.Headers.GetValues(ConfigurationManager.AppSettings["Token"]).First());

                var lstTimesheet = (from rpt in obj.timesheets as IEnumerable<dynamic>
                                    let date = Utils.UnixTimeStampToDateTime(Convert.ToDouble(rpt.timestamp.ToString()))
                                    select
                                    new
                                    {
                                        date = date,
                                        tm =
                                        new Timesheet()
                                            {
                                                TS_Activity = rpt.TS_Activity ?? string.Empty,
                                                TS_Description = rpt.TS_Description ?? string.Empty,
                                                TS_CheckSum = rpt.id,
                                                TS_MachineID =
                                                    rpt.TS_Machine != null ? rpt.TS_Machine.MachineId : 0,
                                                TS_SupervisorID =
                                                    rpt.TS_Supervisor != null
                                                        ? rpt.TS_Supervisor.SupervisorID
                                                        : "0",
                                                TS_Latitude = rpt.latitude,
                                                TS_Longitude = rpt.longitude,
                                                TS_FinishTime = rpt.TS_Finish ?? string.Empty,
                                                TS_StartTime = rpt.TS_Start ?? string.Empty,
                                                TS_Task = rpt.TS_Task ?? string.Empty,
                                                TS_MasterSiteID =
                                                    rpt.TS_Site != null ? rpt.TS_Site.MasterSiteId : 0,
                                                TS_JobID = 0,
                                                TS_IsChecked = rpt.Checked != "False" ? 1 : 0,
                                                TS_ContactID = 0,
                                                TS_IsApproved = 0,
                                                TS_RateCode = rpt.TS_Rate ?? string.Empty,
                                                TS_IsDeleted = 0,
                                                TS_IsDisputed = 0,
                                                TS_IsLocked = 0,
                                                TS_IsReconciled = 0,
                                                TS_IsSubmitted = 0,
                                                TS_TimeStamp = string.Format("{0:yyyy-MM-dd hh:mm:ss}", date),
                                                TS_TotalTime = 0,
                                                TS_Name =
                                                    string.Format(
                                                        "NavCheck_{0}{1}TimeSheet.csv",
                                                        id.ToString("D6"),
                                                        string.Format(
                                                            OperatorRepository.FileDateFormat,
                                                            DateTime.Now)),
                                                TS_Entry = "0",
                                                TS_Date = string.Format("{0:d-MMM-yy}", date)
                                            }
                                    }).ToList();

                foreach (var t in lstTimesheet)
                {
                    this.operatorRepository.SaveTimesheet(id, t.tm, t.date);
                }
                
            }
            catch (Exception exception)
            {
                File.AppendAllText(NavaceRepositoryParserExtension.LogFilePath, string.Format("{0}: {1}\r\n", DateTime.Now.ToShortTimeString(), exception.Message));
                throw exception;
            }
        }
    }
}
