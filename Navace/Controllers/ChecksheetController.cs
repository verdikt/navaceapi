﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChecksheetController.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the ChecksheetController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Web.Http;

    using Microsoft.Owin.Security.Provider;

    using Navace.Domain;
    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities;
    using Navace.Domain.Entities.Upload;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using user = Navace.Domain.Entities.User;

    /// <summary>
    /// The check sheet controller.
    /// </summary>
    public class ChecksheetController : ApiController
    {
        /// <summary>
        /// The _operator repository.
        /// </summary>
        private readonly OperatorRepository operatorRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChecksheetController"/> class. 
        /// </summary>
        /// <param name="operatorRepository">
        /// The operator repository.
        /// </param>
        public ChecksheetController(OperatorRepository operatorRepository)
        {
            this.operatorRepository = operatorRepository;
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="obj">
        /// The object sent.
        /// </param>
        public void Post(dynamic obj)
        {

            var id = user.GetIdByToken(Request.Headers.GetValues(ConfigurationManager.AppSettings["Token"]).First());
            try
            {
                IEnumerable<dynamic> chk = obj.checksheets;

                
                foreach (var c in chk)
                {
                    var checksheetsItems = new List<ChecksheetItem>();
                    var checksheetsPics = new List<Photo>();

                    var machineId = c.machine != null ? c.machine.MachineId.ToString() : "0";
                    double timestamp = 0;
                    double.TryParse(c.timestamp.ToString(), out timestamp);
                    var date = Utils.UnixTimeStampToDateTime(timestamp);
                    var chkName = "NavCheck_" + id.ToString("D6")
                                  + string.Format(OperatorRepository.FileDateFormat, date);


                    var checksheet =
                        new Checksheet()
                        {
                            Cs_CheckSum = c.timestamp.ToString(),
                            Cs_OperatorOk = c.operatorOk != null ? "1" : "0",
                            Cs_MachineRef = machineId.ToString(),
                            Cs_ComponentCode = c.ComponentCode == null ? "7520" : c.ComponentCode.ToString(),
                            Cs_Date = string.Format("{0:d-MMM-yy}", date),
                            Cs_ItemCount = c.items != null ? c.items.Count.ToString() : "0",
                            Cs_MachineOk = c.MachineOk != null ? "False" : "True",
                            Cs_MasterSiteRef = c.site != null ? c.site.MasterSiteId.ToString() : "0",
                            Cs_Name = chkName,
                            Cs_OperatorRef = id.ToString(),
                            Cs_Smu = c.smu != null ? c.smu.ToString() : "0",
                            Cs_Time = string.Format("{0:HH:mm:ss}", date),
                            Cs_JobRef = "0",
                            Cs_Latitude = c.latitude == null ? "0" : c.latitude.ToString(),
                            Cs_Longitude = c.longitude == null ? "0" : c.longitude.ToString()
                        };


                    if (c.jobs == null)
                    {
                        continue;
                    }

                    var itemNumber = 0;
                    foreach (var i in c.items)
                    {
                        itemNumber++;
                        checksheetsItems.Add(
                            new ChecksheetItem()
                                {
                                    Csi_Description = i.Memo.ToString(),
                                    Csi_ItemNumber = itemNumber.ToString(CultureInfo.InvariantCulture),
                                    Csi_ChecksheetName = chkName,
                                    Csi_CheckListItemRef = i.ChecklistId.ToString(),
                                    Csi_CheckSum = Guid.NewGuid().ToString(),
                                    Csi_TimeStamp =
                                        string.Format(
                                            "{0:yyyy-MM-dd hh:mm:ss}",
                                            Utils.UnixTimeStampToDateTime(Convert.ToDouble(i.timestamp))),
                                    Csi_Comment = i.comments == null ? string.Empty : i.comments.ToString(),
                                    Csi_PhotoCount = i.pics != null ? i.pics.Count.ToString() : "0",
                                    Csi_Status = i.status == null ? "0" : i.status.ToString(),
                                });
                        if (i.pics == null)
                        {
                            continue;
                        }

                        var picNumber = 0;
                        foreach (var p in i.pics)
                        {
                            picNumber++;
                            var pictureName = "IMG_NavCheck_" + id.ToString("D6")
                                              + string.Format(OperatorRepository.FileDateFormat, DateTime.Now)
                                              + itemNumber.ToString("D3") + "-" + picNumber + ".jpg";
                            var uploadFilePath =
                                Path.Combine(
                                    ConfigurationManager.AppSettings["PhotoPath"].ToString(CultureInfo.InvariantCulture),
                                    pictureName);

                            NavaceRepositoryParser.SaveImage(p.ToString(), uploadFilePath);
                            checksheetsPics.Add(
                                new Photo()
                                    {
                                        CspCheckSum = "null",
                                        CspChecksheetName = chkName,
                                        CspItemNumber = itemNumber.ToString(CultureInfo.InvariantCulture),
                                        CspPhotoName = pictureName
                                    });
                        }
                    }
                    this.operatorRepository.SaveChecksheet(id, checksheet, date);
                    this.operatorRepository.SaveChecksheetItems(id, checksheetsItems, date);
                    this.operatorRepository.SaveChecksheetPhotos(id, checksheetsPics, date);
                }
            }
            catch (Exception exception)
            {

                File.AppendAllText(NavaceRepositoryParserExtension.LogFilePath, string.Format("{0}: {1}\r\n", DateTime.Now.ToShortTimeString(), exception.Message));
                File.AppendAllText(NavaceRepositoryParserExtension.LogFilePath, exception.StackTrace);
                var json = JsonConvert.SerializeObject(obj);
                File.AppendAllText(NavaceRepositoryParserExtension.LogFilePath, json);
                throw new Exception(NavaceRepositoryParserExtension.LogFilePath, exception);
            }

        }

        // PUT api/checksheet/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/checksheet/5
        public void Delete(int id)
        {
        }
    }

    public class ChkPkg
    {
        public DateTime date;
        public Checksheet chk;
    }
}
