﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportController.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the ReportController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Navace.Domain;

namespace Navace.Controllers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection.Emit;
    using System.Web.Http;

    using Navace.Domain.Abstractions;
    using Navace.Domain.Entities;
    using Navace.Domain.Entities.Upload;

    using user = Navace.Domain.Entities.User;

    /// <summary>
    /// The report controller.
    /// </summary>
    public class ReportController : ApiController
    {
        /// <summary>
        /// The operator repository.
        /// </summary>
        private readonly OperatorRepository operatorRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportController"/> class.
        /// </summary>
        /// <param name="operatorRepository">
        /// The operator Repository.
        /// </param>
        public ReportController(OperatorRepository operatorRepository)
        {
            this.operatorRepository = operatorRepository;
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        public void Post(dynamic obj)
        {
            try
            {
                var datenow = DateTime.Now;
                var id = user.GetIdByToken(Request.Headers.GetValues(ConfigurationManager.AppSettings["Token"]).First());
                var name = string.Format(
                    "NavCheck_{0}{1}_IncidentReport.csv",
                    id.ToString("D6"),
                    string.Format(OperatorRepository.FileDateFormat, datenow));
                //var lstPhot = new List<Photo>();
                var index = 0;
                var ip = 0;
                var lstReport = (from rpt in obj.reports as IEnumerable<dynamic>
                                 let date = Utils.UnixTimeStampToDateTime(Convert.ToDouble(rpt.timestamp))
                                 let pics = (from p in rpt.pics as IEnumerable<dynamic>
                                             let pictureName =
                                                 "IMG _" + id.ToString("D6")
                                                 + string.Format(OperatorRepository.FileDateFormat, DateTime.Now)
                                                 + (ip++).ToString("D3") + ".jpg"
                                             select
                                                 (new Photo
                                                      {
                                                          CspCheckSum = name,
                                                          CspChecksheetName = name,
                                                          CspItemNumber =
                                                              index.ToString(CultureInfo.InvariantCulture),
                                                          CspPhotoName =
                                                              id.ToString("D6")
                                                              + string.Format(
                                                                  OperatorRepository.FileDateFormat,
                                                                  DateTime.Now) + (index++) + ".jpg"
                                                      }).Set(
                                                          x =>
                                                          {
                                                              var uploadFilePath =
                                                                  Path.Combine(
                                                                      ConfigurationManager
                                                                          .AppSettings["PhotoPath"]
                                                                          .ToString(
                                                                              CultureInfo
                                                                                  .InvariantCulture),
                                                                      x.CspPhotoName);

                                                              NavaceRepositoryParser.SaveImage(
                                                                  p.ToString(),
                                                                  uploadFilePath);
                                                              //lstPhot.Add(x);
                                                          })).ToList()
                                 select
                                     new
                                     {
                                         pics = pics,
                                         date = date,
                                         report = new IncidentReport()
                                             {
                                                 Incident_CheckSum = rpt.id,
                                                 Incident_MachineRef =
                                                     rpt.machine != null
                                                         ? rpt.machine.MachineId
                                                         : string.Empty,
                                                 Incident_Category = rpt.criticity,
                                                 Incident_ComponentCode = rpt.componentCode,
                                                 Incident_ActionRequired = rpt.serviceman,
                                                 Incident_Date =
                                                     string.Format("{0:dd/MM/yyyy}", (DateTime)date),
                                                 Incident_Time =
                                                     string.Format("{0:hh:mm tt}", (DateTime)date),
                                                 Incident_Description = rpt.description,
                                                 Incident_Latitude = rpt.latitude,
                                                 Incident_Longitude = rpt.longitude,
                                                 Incident_MasterSiteRef =
                                                     rpt.site != null ? rpt.site.MasterSiteId : string.Empty,
                                                 Incident_Name = name,
                                                 Incident_OperatorRef =
                                                     id.ToString(CultureInfo.InvariantCulture),
                                                 Incident_PhotoCount =
                                                     rpt.pics != null ? rpt.pics.Count.ToString() : 0,
                                                 Incident_Response = rpt.response,
                                                 Incident_Severity = rpt.severity,
                                                 Incident_Subcategory = rpt.subcategory
                                             }
                                     }).ToList();

                
                foreach (var r in lstReport)
                {
                    this.operatorRepository.SaveReportIncident(id, r.report, r.date);
                    this.operatorRepository.SaveReportPhotos(id, r.pics, r.date);
                }

                
            }
            catch (Exception exception)
            {
                File.AppendAllText(NavaceRepositoryParserExtension.LogFilePath, string.Format("{0}: {1}\r\n", DateTime.Now.ToShortTimeString(), exception.Message));
                throw exception;
            }
            //this.operatorRepository.
        }
    }
}
