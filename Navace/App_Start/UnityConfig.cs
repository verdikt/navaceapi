using Microsoft.Practices.Unity;
using System.Web.Http;
using Navace.Domain.Abstractions;
using Navace.Domain.Entities;
using Unity.WebApi;

namespace Navace
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();

            container
                .RegisterType<IRepository<Operator>, OperatorRepository>()
                .RegisterType<IRepository<User>, UserRepository>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}