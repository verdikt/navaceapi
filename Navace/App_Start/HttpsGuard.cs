// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpsGuard.cs" company="Verdikt">
//   
// </copyright>
// <summary>
//   The https guard.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// The https guard.
    /// </summary>
    public class HttpsGuard : DelegatingHandler
    {
        /// <summary>
        /// The send async.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.RequestUri.Scheme.Equals(Uri.UriSchemeHttps, StringComparison.OrdinalIgnoreCase))
            {
                return base.SendAsync(request, cancellationToken);
            }

            var reply = request.CreateErrorResponse(HttpStatusCode.BadRequest, "HTTPS is required for security reason.");
            return Task.FromResult(reply);
        }
    }
}