﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="Verdikt">
//   
// </copyright>
// <summary>
//   The web API config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace
{
    using System.Collections.Generic;
    using System.Web.Cors;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using System.Web.Http.Dispatcher;

    /// <summary>
    /// The web API config.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// The register.
        /// </summary>
        /// <param name="config">
        /// The config.
        /// </param>
        public static void Register(HttpConfiguration config)
        {

            UnityConfig.RegisterComponents();

            var tokenInspector = new TokenInspector() { InnerHandler = new HttpControllerDispatcher(config) };
           

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Authentication",
                routeTemplate: "api/auth/{id}",
                defaults: new { controller = "auth" });


            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: null,
                handler: tokenInspector);

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.MessageHandlers.Add(new HttpsGuard());

            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}
