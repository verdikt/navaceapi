﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Utils.cs" company="Verdikt">
//   2014
// </copyright>
// <summary>
//   Defines the Utils type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Navace
{
    using System;

    /// <summary>
    /// The utils.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// The unix time stamp to date time.
        /// </summary>
        /// <param name="unixTimeStamp">
        /// The unix time stamp.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dateTime = dateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }

        public static T Set<T>(this T input, Action<T> updater)
        {
            updater(input);
            return input;
        }
    }
}